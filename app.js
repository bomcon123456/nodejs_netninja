let express = require('express');
let todoController = require('./controllers/todoController')

let app = express();

// Set-up template engine
app.set('view engine', 'ejs');

// Static-file
app.use(express.static('./public'));

// Fire controllers
todoController(app);

// Listen to port

app.listen(3000);
console.log('App is running on port 3000.');