var bodyParser = require('body-parser');
var mongoose = require('mongoose');

// Conect to database
mongoose.connect('mongodb+srv://test:test@todo-ejl26.mongodb.net/test?retryWrites=true', {
    useNewUrlParser: true
});

// Schema
var todoSchema = new mongoose.Schema({
    item: String
});

var Todo = mongoose.model('Todo', todoSchema);

var urlencodedParser = bodyParser.urlencoded({
    extended: false
});

module.exports = function (app) {
    app.get('/todo', function (req, res) {
        // Get all data of Todo Model.
        Todo.find({}, function (err, data) {
            if (err) throw error;
            res.render('todo', {
                todos: data
            })
        });
    });

    app.post('/todo', urlencodedParser, function (req, res) {
        let newTodo = Todo(req.body).save(function (err, data) {
            if (err) throw err;
            res.json(data);
        });
    });

    app.delete('/todo/:item', function (req, res) {
        Todo.find({
            item: req.params.item.replace(/\-/g, " ")
        }).remove(function(err, data) {
            if(err) throw err;
            res.json(data);
        });
    });
};